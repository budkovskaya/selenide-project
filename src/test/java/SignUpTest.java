import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Configuration;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.$;

public class SignUpTest {

    SignUpPage page = new SignUpPage();

    @BeforeClass
    public static void setUp() {
        // Property baseUrl, которое хранится в классе Configuration
        Configuration.baseUrl = "https://www.spotify.com";
        // Property browser, которое хранится в классе Configuration
        Configuration.browser = "firefox";
    }

    // Check that validation message about valid year is displayed if invalid data in entered into Year field
    @Test
    public void typeInvalidYear() {
        new SignUpPage().open()
                .typeEmail("cdjcd@gmail.com")
                .typeConfirmationEmailField("hhhh")
                .typePassword("dzsds")
                .setMonth("December")
                .typeDay("5")
                .typeYear("75")
                .clickSignUp();
        $(page.getInvalidYearMessage()).shouldBe(Condition.visible);
    }

    // Check that validation message about invalid date is not displayed if valid data in entered into Date field
    @Test
    public void typeValidDay() {
        new SignUpPage().open()
                .typeEmail("cdjcd@gmail.com")
                .typeConfirmationEmailField("hhhh")
                .typePassword("dzsds")
                .setMonth("December")
                .typeDay("5")
                .typeYear("75")
                .clickSignUp();
        $(page.getInvalidDayMessage()).shouldNotBe(Condition.visible);
    }

    // Check that validation message about invalid date is displayed if invalid data in entered into Date field
    @Test
    public void typeInvalidDate() {
        new SignUpPage().open()
                .typeEmail("cdjcd@gmail.com")
                .typeConfirmationEmailField("hhhh")
                .typePassword("dzsds")
                .setMonth("December")
                .typeDay("345")
                .typeYear("2000")
                .clickSignUp();
        $(page.getInvalidDayMessage()).should(Condition.appear);
    }

    // Check that validation message about valid year is not displayed if valid data in entered into Year field
    @Test
    public void typeValidYear() {
        new SignUpPage().open()
                .typeEmail("cdjcd@gmail.com")
                .typeConfirmationEmailField("hhhh")
                .typePassword("dzsds")
                .setMonth("December")
                .typeDay("3")
                .typeYear("2000")
                .clickSignUp();
        $(page.getInvalidYearMessage()).shouldNot(Condition.appear);
    }

    // Check that validation message that password is too short is displayed if password < 8 characters is entered
    @Test
    public void typeShortPassword() {
        new SignUpPage().open()
                .typeEmail("cdjcd@gmail.com")
                .typeConfirmationEmailField("hhhh")
                .typePassword("dzsds")
                .clickSignUp();
        $(page.getInvalidPasswordMessage()).shouldHave(Condition.text("Your password is too short."));
    }

    // Check that validation message about password not being entered is not displayed if any value is entered into Password field
    @Test
    public void checkPasswordFieldNotEmpty() {
        new SignUpPage().open()
                .typeEmail("cdjcd@gmail.com")
                .typeConfirmationEmailField("hhhh")
                .typePassword("dzsds")
                .clickSignUp();
        $(page.getInvalidPasswordMessage()).shouldNotHave(Condition.text("Enter a password to continue."));
    }

}
