import com.codeborne.selenide.Selenide;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static org.openqa.selenium.By.cssSelector;

public class SignUpPage {

    public final static String relativeOrAbsoluteUrl = "/us/signup";
    private By emailFriend = cssSelector("#register-email");
    private By confirmEmailField = cssSelector("#register-confirm-email");
    private By passwordField = cssSelector("#register-password");
    private By displayNameField = cssSelector("#register-displayname");
    private By monthDropDown = cssSelector("#register-dob-month");
    private By dayField = cssSelector("#register-dob-day");
    private By yearField = cssSelector("#register-dob-year");
    private By shareCheckbox = cssSelector("#register-thirdparty");
    private By registerButton = cssSelector("#register-button-email-submit");
    private By invalidYearMessage = cssSelector("label[for='register-dob-year']");
    private By invalidDayMessage = cssSelector("label[for='register-dob-day']");
    private By invalidPasswordMessage = cssSelector("label[for='register-password'][class='has-error']");

    public By getInvalidYearMessage() {
        return invalidYearMessage;
    }

    public By getInvalidDayMessage() {
        return invalidDayMessage;
    }

    public By getInvalidPasswordMessage() {
        return invalidPasswordMessage;
    }

    public SignUpPage open() {
        //Указываем обратный путь
        Selenide.open(relativeOrAbsoluteUrl);
        return this;
    }

    //Метод для заполнения поля email
    public SignUpPage typeEmail(String email) {
        $(emailFriend).setValue(email);
        return this;
    }

    //Метод для заполнения поля Confirm email
    public SignUpPage typeConfirmationEmailField(String email) {
        $(confirmEmailField).setValue(email);
        return this;
    }

    //Метод для заполнения поля password
    public SignUpPage typePassword(String password) {
        $(passwordField).val(password);
        return this;
    }

    //Метод выбора месяца
    public SignUpPage setMonth(String month) {
        $(monthDropDown).selectOption(month);
        return this;
    }

    //Метод для заполнения поля Day
    public SignUpPage typeDay(String day) {
        $(dayField).sendKeys(day);
        return this;
    }

    //Метод для заполнения поля Year
    public SignUpPage typeYear(String year) {
        $(yearField).sendKeys(year);
        return this;
    }

    public SignUpPage clickSignUp() {
        $(registerButton).click();
        return this;
    }

}